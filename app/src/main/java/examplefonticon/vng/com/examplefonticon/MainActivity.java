package examplefonticon.vng.com.examplefonticon;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontHelper.init(getAssets());
        setContentView(R.layout.activity_main);

        Typeface font = Typeface.createFromAsset( getAssets(), "fonts/zalopay.ttf" );

        TextView tvLocation = (TextView) findViewById(R.id.iconLocation);
        tvLocation.setTypeface(font);
        tvLocation.setText(R.string.location);

        TextView tvBeer = (TextView) findViewById(R.id.iconBeer);
        tvBeer.setTypeface(font);
        tvBeer.setText(R.string.beer);
    }
}
